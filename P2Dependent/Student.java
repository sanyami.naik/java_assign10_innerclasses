package Assignments.ten.P2Dependent;
import Assignments.ten.P1Independent.Address;

public class Student {

    String name;
    int rollNo;

    Address address;

    public Student(String name, int rollNo, Address address) {
        this.name = name;
        this.rollNo = rollNo;
        this.address = address;
    }

    void studentDisplay()
    {
        System.out.println("The student details are ");
        System.out.println("name:"+name);
        System.out.println("roll no: "+rollNo);
        System.out.println("address: "+address);
    }


    public static void main(String[] args) {
        //method 1 using anonymous object
        Student s1=new Student("Sanyami",15,new Address(12,"MG Road","Mahad",402309));
        s1.studentDisplay();

        Address a1=new Address();
        //a1.msg();               when its private it stays only open to class

        //a1.msg();               when its default it wont run as default can never be accesed outside package

        //a1.msg();               when its protected we need to have inheritance then it can be accesed

        a1.msg();               //when its public it can be accesed outside the anywhere


        System.out.println("*****************************************************");


        //method 1 using reference object
        Address a2=new Address(10,"DP Road","Mumbai",675481);
        Student s2=new Student("Sanyamiii",25,a2);
        s2.studentDisplay();
        System.out.println(a2);

    }
}




/*
Output
The student details are
name:Sanyami
roll no: 15
address: The address details are :Flat No 12, streetNameMG Road, pincode 402309
MESSAGE
*****************************************************
The student details are
name:Sanyamiii
roll no: 25
address: The address details are :Flat No 10, streetNameDP Road, pincode 675481
The address details are :Flat No 10, streetNameDP Road, pincode 675481

 */