package Assignments.ten.InnerClasses;
class outer
{
    void method()
    {
        class MethodInner
        {
            void inMethod()
            {
                System.out.println("In the method of methodlocal inner class");
            }
//            MethodInner m=new MethodInner();                   this will not be reachable.
//            m.inMethod();
        }
        MethodInner m=new MethodInner();
        m.inMethod();
    }
}
public class MethodLocal {

    public static void main(String[] args) {
        outer o=new outer();
        o.method();
    }


}




/*
OUTPUT
In the method of methodlocal inner class

 */