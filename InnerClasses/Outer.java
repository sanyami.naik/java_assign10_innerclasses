package Assignments.ten.InnerClasses;

class Outer{
    int a=4;
    class Inner{
        void innerDisplay(){
            System.out.println("INNER DISPLAY");
            System.out.println(a);
        }
    }



    public static void main(String[] args) {
        //method1
        Outer.Inner i1=new Outer().new Inner();
        i1.innerDisplay();


        System.out.println("*************************************");

        //method2
        Outer o=new Outer();
        Outer.Inner i2=o.new Inner();
        i2.innerDisplay();

    }
}
