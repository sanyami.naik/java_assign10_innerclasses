package Assignments.ten.InnerClasses;

public class StaticInner {

        static class statClass{
              static void display() {
                  System.out.println("In the static inner class static method");
              }

              void display2()
              {
                  System.out.println("In the static inner class instance method");
              }
        }

    public static void main(String[] args) {

            StaticInner.statClass.display();           //as display is static
            statClass sc=new statClass();
            sc.display2();                            //as display2 is instance method of statclass
    }
}


/*
OUTPUT
In the static inner class static method
In the static inner class instance method
 */