package Assignments.ten.InnerClasses;

class Basic{
    public void show()
    {

    }
    public void out()
    {
        System.out.println("Heyyy in basic out");
    }
}
public class AnonymousInner {
    public static void main(String[] args) {
        Basic b=new Basic(){
               public void show(){
                   System.out.println("overidden show");
               }

               public void display()
               {
                   System.out.println("new display");
               }
        };

        //b.display();                   you cannot add new methods in anonymous class is the difference betwen inner and it.
        b.out();
        b.show();
    }
}
