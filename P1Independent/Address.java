package Assignments.ten.P1Independent;


public class Address {

    int flatNo;
    String streetName;
    String cityName;
    int pincode;

    public Address(){

    }
    public Address(int flatNo, String streetName, String cityName, int pincode) {
        this.flatNo = flatNo;
        this.streetName = streetName;
        this.cityName = cityName;
        this.pincode = pincode;
    }

    public void msg(){
        System.out.println("MESSAGE");
    }

    public String toString()
    {
        return ("The address details are :Flat No "+flatNo+", streetName"+streetName+", pincode "+pincode);
    }
}
